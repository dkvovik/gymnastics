<?php
/**
 * Скрипт для отпраки email уведомлений
 */

/**список получателей через запятую*/
$to = "insale@europegym.ru";

/**отправитель*/
$from = 'no-reply@europegym.ru';

//Тема письма
$subject = "Новая заявка с сайта ".$_SERVER['SERVER_NAME'];

//имя клиента
$name = trim($_POST["name"]);

//телефон клиента
$phone = trim($_POST["phone"]);

//email клиента
$email = trim($_POST["email"]);

//возраст ребенка
$age = trim($_POST["age"]);

//тема заявки
$title = trim($_POST["title"]);

$message = "";

if ($title){
  $message .= "<b>Тема заявки:</b> $title <br>";
}

if ($name){
  $message .= "<b>Имя клиента:</b> $name <br>";
}

if ($phone){
  $message .= "<b>Телефон клиента:</b> $phone <br>";
}

if ($email){
  $message .= "<b>E-mail:</b> $email <br>";
}

if ($age){
  $message .= "<b>Возраст ребенка:</b> $age <br>";
}

if($message) {
    $headers = "From: $from\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=utf-8\r\n";
    echo mail($to, "=?utf-8?B?".base64_encode($subject)."?=", $message, $headers, "-f ".$from);
}
?>
