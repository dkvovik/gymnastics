# Workflow for landing-page site

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

### Dependencies

* Pug
* Scss
* EsLint - standart
* Imagemin
* Babel
* Critical css