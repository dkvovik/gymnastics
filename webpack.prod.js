const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CriticalPlugin = require('webpack-plugin-critical').CriticalPlugin
const autoprefixer = require('autoprefixer')

module.exports = merge(common, {
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|static)/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							'presets': [
								['env', {
									'modules': false,
									'targets': {
										'browsers': ['> 1%', 'last 2 versions', 'not ie <= 8']
									}
								}],
								'stage-3'
							]
						}
					}
				]
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {
								minimize: true
							}
						},
						{
							loader: 'group-css-media-queries-loader'
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: [
									autoprefixer({
										browsers: [ '> 1%', 'last 2 versions', 'not ie <= 8' ]
									})
								]
							}
						},
						{
							loader: 'sass-loader'
						}
					]
				})
			},
			{
				test: /\.(gif|jpe?g|png|svg|webp)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'img/[path][name].[ext]',
							context: 'src/img'
						}
					},
					{
						loader: 'image-webpack-loader',
						options: {
							mozjpeg: {
								progressive: true,
								quality: 65
							},
							// optipng.enabled: false will disable optipng
							optipng: {
								enabled: true
							},
							pngquant: {
								quality: '65-90',
								speed: 4
							},
							gifsicle: {
								interlaced: false
							}
							// webp: {
							//   quality: 75
							// }
						}
					}
				]
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('styles.css'),
		new CriticalPlugin({
			src: 'index.html',
			inline: true,
			minify: true,
			dest: 'index.html'
		})
	]
})
