const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
	plugins: [
		new CleanWebpackPlugin(['dist']),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: path.resolve(__dirname, 'src/index.pug')
		}),
		new CopyWebpackPlugin([
			{
				from: path.resolve(__dirname, 'static/favicon'),
				to: path.resolve(__dirname, 'dist/img/favicon'),
				ignore: ['.*']
			},
			{
				from: path.resolve(__dirname, 'static/php'),
				to: path.resolve(__dirname, 'dist/php'),
				ignore: ['.*']
			},
			{
				from: path.resolve(__dirname, 'static/privacy_policy.pdf'),
				to: path.resolve(__dirname, 'dist'),
				ignore: ['.*']
			},
			{
				from: path.resolve(__dirname, 'static/og.jpg'),
				to: path.resolve(__dirname, 'dist/img/'),
				ignore: ['.*']
			}
		]),
		new ExtractTextPlugin('styles.css'),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		})
	],
	module: {
		rules: [
			{
				test: /\.pug$/,
				use: [
					{
						loader: 'pug-loader',
						options: {
							pretty: false
						}
					}
				]
			},
			{
				test: /\.(woff|woff2)$/,
				loader: 'file-loader',
				options: {
					name: 'fonts/[name].[ext]'
				}
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader'
						}
					]
				})
			},
			{
				test: path.resolve(__dirname, 'node_modules/wowjs/dist/wow.min.js'),
				loader: 'exports?this.WOW'
			}
		]
	}
}
