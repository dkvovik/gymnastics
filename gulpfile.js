const gulp = require('gulp')
const webpConvert = require('gulp-webp')
const svgo = require('gulp-svgo')

gulp.task('webp', () =>
	gulp.src('src/**/*/*{jpg,png}')
		.pipe(webpConvert({
			method: 6,
			quality: 75
		}))
		.pipe(gulp.dest('src/'))
)

gulp.task('svg', () => {
	gulp.src('src/**/*/*.svg')
		.pipe(svgo())
		.pipe(gulp.dest('src/'))
})
