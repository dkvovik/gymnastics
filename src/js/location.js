/*eslint-disable*/
ymaps.ready(init)

function init () {
	// Создание экземпляра карты.
	var myMap = new ymaps.Map('map', {
			center: [55.722289, 37.547477],
			zoom: 16,
      controls: ['zoomControl']
		}),
		// Контейнер для меню.
		menu = $('<ul class="menu menu-address"></ul>'),
		collection = new ymaps.GeoObjectCollection(null, { preset: groups.style })
  
  myMap.behaviors.disable('scrollZoom');
  myMap.geoObjects.add(collection)

	for (var j = 0, m = groups.items.length; j < m; j++) {
		createMenuGroup(groups.items[j])
	}

	function createMenuGroup (item) {
		var menuItem = $(
				`<li class="menu-address__item">\n
          <p class="top"><span class="title">${item.name}</span><a href="#">Показать на карте</a></p>\n
          <p class="bottom">${item.address}</p>\n
        </li>`),
			placemark = new ymaps.Placemark(item.center,
        {
          balloonContent: `
            <b>${item.address}</b><br>
            ${item.phone}<br>
            ${item.email} 
          `
        },
        {
          iconLayout: 'default#image',
          iconImageHref: 'img/baloon-map.png',
          iconImageSize: [50, 55],
          iconImageOffset: [-22, -55]
        })

		// Добавляем метку в коллекцию.
		collection.add(placemark)

		menuItem
			.appendTo(menu)
			.find('a')
			.bind('click', function () {
				if (!placemark.balloon.isOpen()) {
					placemark.balloon.open()
				} else {
					placemark.balloon.close()
				}
				return false
			})
	}

	// Добавляем меню в тэг BODY.
	menu.appendTo($('.address-menu-wrapper'))
	// Выставляем масштаб карты чтобы были видны все группы.
}

// Адреса центров
var groups = {
	name: 'Известные памятники',
	style: 'islands#redIcon',
	items: [
		{
			center: [55.722289, 37.547477],
			name: 'Лужники',
			address: 'Москва, ул. Лужники, дом 24, стр.2.',
			phone: '+7 (499) 521 50 79, доб. 110',
      email: 'luzhniki@europegym.ru'
		},
		{
			center: [55.802482, 37.743660],
			name: 'Локомотив',
			address: 'Москва, Большая Черкизовская, 125 А, стр.4',
      phone: '+7 (499) 521 50 79, доб. 120',
      email: 'loko@europegym.ru'
		},
		{
			center: [55.619183, 37.393407],
			name: 'Киевское шоссе',
			address: 'Киевское шоссе 24-й км от МКАД, дер. Рогозинино',
      phone: '+7 (499) 521 50 79, доб. 130',
      email: 'newmoscow@europegym.ru'
		},
		{
			center: [55.719348, 37.407070],
			name: 'Сетунь',
			address: 'Москва, ул. Кубинка, 7',
      phone: '+7 (499) 521 50 79, доб. 140',
      email: 'setun@europegym.ru'
		},
		{
			center: [55.889937, 37.388322],
			name: 'Куркино',
			address: 'Москва, ул.Воротынская, 18',
      phone: '+7 (499) 521 50 79, доб. 150',
      email: 'kurkino@europegym.ru'
		},
		{
			center: [55.790200, 37.544647],
			name: 'Динамо',
			address: 'Москва, Ленинградский проспект, 37Б',
      phone: '+7 (499) 521 50 79, доб. 100',
      email: 'dinamo@europegym.ru'
		},
		{
			center: [55.600974, 37.609191],
			name: 'Чертаново',
			address: 'Москва, Россошанский проезд, 3',
      phone: '+7 (499) 521 50 79, доб. 160',
      email: 'chertanovo@europegym.ru'
		},
		{
			center: [55.649825, 37.770043],
			name: 'Марьино',
			address: 'Москва, ул.Поречная, 10',
      phone: '+7 (499) 521 50 79, доб. 170',
      email: 'mari@europegym.ru'
		},
		{
			center: [55.885963, 37.678460],
			name: 'Медведково',
			address: 'Москва, ул. Широкая, 30',
      phone: '+7 (499) 521 50 79, доб. 180',
      email: 'medvedkovo@europegym.ru'
		},
		{
			center: [55.803479, 37.390514],
			name: 'Строгино',
			address: 'Москва, ул. Кулакова, 20к1',
      phone: '+7 (499) 521 50 79, доб. 190',
      email: 'strogino@europegym.ru'
		},
		{
			center: [55.685137, 37.877392],
			name: 'Люберцы',
			address: 'г. Люберцы, Октябрьский проспект 112, ТЦ Выходной, 2этаж',
      phone: '+7 (495) 638 09 05',
      email: 'lubertsy@europegym.ru'
		},
		{
			center: [55.921491, 37.718372],
			name: 'Мытищи',
			address: 'г. Мытищи, ул. Мира, стр № 32/2, ТЦ 4Daily, 3 этаж',
      phone: '+7 (495) 638 09 07',
      email: 'mytischi@europegym.ru'
		}
	]
}
